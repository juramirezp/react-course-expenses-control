import React from 'react'
import PropTypes from 'prop-types'


const Error = ({mesage}) => (
    <p className="alert alert-danger error">{mesage}</p>
)

Error.propTypes = {
    mesage: PropTypes.string.isRequired
}
 
export default Error;