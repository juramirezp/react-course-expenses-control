import React, {Fragment, useState} from 'react';
import Error from './Error'
import PropTypes from 'prop-types'


const Question = ({setDifference, setTotalBudget, setShowQuestion}) => {

    // Creacion de state
    const [budget, setBudget] = useState(0);
    const [error, setError] = useState(false);

    // Setear monto presupuesto
    const setBugdetAmount = e => {
        setBudget(parseInt(e.target.value, 10));
    }

    // Definir presupuesto
    const saveBudget = e => {
        e.preventDefault();

        if(budget < 1 || isNaN(budget)){
            setError(true);
            return;
        }else{
            setError(false);
            setTotalBudget(budget)
            setDifference(budget)
            setShowQuestion(false)
        }
    }

    return (
        <Fragment>
            <h2>Ingresa tu presupuesto</h2>

            {error ? <Error mesage="El monto es incorrecto"/> : null}

            <form>
                <input
                    type="number"
                    className="u-full-width"
                    placeholder="Presupuesto"
                    onChange={setBugdetAmount}
                />

                <input
                    type="submit"
                    className="button-primary u-full-width"
                    value="Definir presupuesto"
                    onClick={saveBudget}
                />
            </form>
        </Fragment>
      );
}
 
Question.propTypes ={
    setDifference: PropTypes.func.isRequired,
    setTotalBudget: PropTypes.func.isRequired,
    setShowQuestion: PropTypes.func.isRequired
}

export default Question;
