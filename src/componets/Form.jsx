import React, {useState} from 'react';
import Error from './Error';
import shortid from 'shortid';
import PropTypes from 'prop-types'


const Form = ({setSpending, setCreateSpending}) => {

    // Definir States
    const [name, setName] = useState('');
    const [amount, setAmount] = useState(0);
    const [error, setError] = useState(false);

    // guardar gasto
    const addSpending = e => {
        e.preventDefault();

        if(amount < 1 || isNaN(amount)  || name.trim() === ''){
            setError(true);
            return;
        }else{
            setError(false);

            // Crear gasto
            const spending = {
                name,
                amount,
                id: shortid.generate()
            }

            // Enviar gasto
            setSpending(spending);

            // Limpiar formulario
            setName('');
            setAmount(0);
            setCreateSpending(true);
        }
    }

    return ( 
        <form
            onSubmit={addSpending}
        >
            <h2>Agrega tus gastos aquí</h2>

            {error ? <Error mesage="Error en la información ingresada"/> : null}

            <div className="campo">
                <label>Nombre del gasto</label>
                <input 
                    type="text"
                    className="u-full-width"
                    placeholder="Ej: Transporte"    
                    value={name}
                    onChange={e => setName(e.target.value)}
                />
            </div>

            <div className="campo">
                <label>Monto</label>
                <input 
                    type="number"
                    className="u-full-width"
                    placeholder="Ej: 300"
                    value={amount}
                    onChange={e => setAmount( parseInt(e.target.value, 10))}
                />
            </div>

            <input 
                type="submit" 
                className="button-primary u-full-width" 
                value="Agregar Gasto" 
            />
        </form>
     );
}

Form.propTypes ={
    setSpending: PropTypes.func.isRequired,
    setCreateSpending: PropTypes.func.isRequired
}
 
export default Form;