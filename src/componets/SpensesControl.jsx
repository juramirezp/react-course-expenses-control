import React, {Fragment} from 'react'
import {checkBudget} from '../helpers'
import PropTypes from 'prop-types'


const  SpensesControl= ({totalBudget, difference}) => {
    return (
        <Fragment>
            <div className="alert alert-primary">
                Presupuesto: $ {totalBudget}
            </div>
            <div className={checkBudget(totalBudget, difference)} >
                Disponible: $ {difference}
            </div>
        </Fragment>
      );
}

SpensesControl.propTypes = {
    totalBudget: PropTypes.number.isRequired,
    difference: PropTypes.number.isRequired
}
 
export default SpensesControl;