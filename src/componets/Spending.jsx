import React, {useState} from 'react'
import PropTypes from 'prop-types'


const Spending = ({spending}) => (
    <li className="gastos">
        <p>
            {spending.name}

            <span className="gasto">$ {spending.amount}</span>
        </p>
    </li>
)

Spending.propTypes ={
    spendings: PropTypes.object.isRequired
}
 
export default Spending;