import React, {useState, useEffect} from 'react';
import Question from './componets/Question'
import Form from './componets/Form'
import List from './componets/List'
import SpensesControl from './componets/SpensesControl'

function App() {

  // Definir States
  const [totalBudget, setTotalBudget] = useState(0);
  const [difference, setDifference] = useState(0);
  const [showQuestion, setShowQuestion] = useState(true);
  const [spendings, setSpendings] = useState([]);
  const [spending, setSpending] = useState([]);
  const [createSpending, setCreateSpending] = useState(false);

  // useEffcet que actualiza monto disponible
  useEffect( () => {
    if(createSpending){

      // Guarda el gasto
      setSpendings([
        ...spendings,
        spending
      ])

      // resta el gasto del total disponible
      setDifference(difference - spending.amount);

      setCreateSpending(false);
    }
  },[spending]);

  return (
    <div className="container">
      <header>
        <h1>Gasto Semanal</h1>
        <div className="contenido-principal contenido">
          {
            showQuestion ?
            (
              <Question
                setTotalBudget={setTotalBudget}
                setDifference={setDifference}
                setShowQuestion={setShowQuestion}
              ></Question>
            )

            : 

            (
              <div className="row">
                <div className="one-half column">
                  <Form
                    setSpending={setSpending}
                    setCreateSpending={setCreateSpending}
                  ></Form>
                </div>
                <div className="one-half column">
                  <List
                    spendings={spendings}
                  ></List>

                  <SpensesControl
                    totalBudget={totalBudget}
                    difference={difference}
                  ></SpensesControl>
                </div>
              </div>
            )
          }

        </div>
      </header>
    </div>
  );
}

export default App;
